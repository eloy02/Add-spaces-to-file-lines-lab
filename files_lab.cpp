﻿#include <iostream>
#include <fstream>
#include <string>

int main()
{
	std::string file_path = "1.txt";
	std::string temp_file_path = "temp.txt";

	std::ifstream fin;
	std::ofstream fout;

	fin.open(file_path, std::fstream::basic_ios::in);
	fout.open("temp.txt", std::fstream::basic_ios::trunc);

	if (!fin.is_open())
	{
		std::cout << "File not found";

		return 0;
	}

	while (!fin.eof())
	{
		std::string buff;

		std::getline(fin, buff);

		int space_count = 0;

		for (auto c : buff)
			if (c == ' ')
				space_count++;

		for (int i = 0; i < space_count; i++)
			buff.insert(0, " ");

		fout << buff << "\n";
	}

	fin.close();
	fout.close();

	fin.open("temp.txt", std::fstream::basic_ios::in);
	fout.open(file_path, std::fstream::basic_ios::trunc);

	while (!fin.eof())
	{
		std::string buff;

		std::getline(fin, buff);

		fout << buff << "\n";
	}

	fin.close();
	fout.close();

	std::remove("temp.txt");
}